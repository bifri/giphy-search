package io.bifri.giphysearch.presentation.main;

import io.bifri.giphysearch.data.rest.giphypojo.GiphyResponse;
import io.bifri.giphysearch.data.rest.repository.GiphyApiClient;
import io.bifri.giphysearch.presentation.IBaseSchedulerProvider;
import io.bifri.giphysearch.util.Log;
import io.bifri.giphysearch.util.rx.DelayStrategy;
import io.bifri.giphysearch.util.rx.RetryWithDelay;
import rx.Observable;
import rx.subscriptions.CompositeSubscription;

import static io.bifri.giphysearch.data.rest.repository.GiphyApiClient.DEFAULT_LIMIT_COUNT;
import static io.bifri.giphysearch.util.rx.DelayStrategy.CONSTANT_DELAY_TIMES_RETRY_COUNT;

/**
 * @author <a href="mailto:jaredsburrows@gmail.com">Jared Burrows</a>
 */

// FIXME: rename packages to io.bifri.giphysearch
final class MainPresenter implements IMainPresenter {
    private final static String TAG = MainPresenter.class.getSimpleName();

    private static final int DEFAULT_RETRY_COUNT = 3;
    private static final int DEFAULT_RETRY_DELAY_MILLIS = 1000;
    private static final DelayStrategy DEFAULT_RETRY_DELAY_STRATEGY =
            CONSTANT_DELAY_TIMES_RETRY_COUNT;
    private final CompositeSubscription compositeSubscription = new CompositeSubscription();
    private final IMainView view;
    private final GiphyApiClient repository;
    private final IBaseSchedulerProvider provider;

    public MainPresenter(IMainView view, GiphyApiClient repository,
                         IBaseSchedulerProvider provider) {
        this.view = view;
        this.repository = repository;
        this.provider = provider;

        this.view.setPresenter(this);
    }

    @Override public void subscribe() {
    }

    @Override public void unsubscribe() {
        compositeSubscription.clear();
    }

    @Override public void clearImages() {
        // Clear current data
        view.clearImages();
    }

    /**
     * Load gif trending images.
     */
    @Override public void loadTrendingImages(int offset) {
        loadImages(repository.trending(DEFAULT_LIMIT_COUNT, offset));
    }

    /**
     * Search gifs based on user input.
     *
     * @param searchString User input.
     */
    @Override
    public void loadSearchImages(String searchString, int offset) {
        loadImages(repository.search(searchString, DEFAULT_LIMIT_COUNT, offset));
    }

    public void loadImages(Observable<GiphyResponse> observable) {
        compositeSubscription.add(observable
                .retryWhen(new RetryWithDelay(
                        DEFAULT_RETRY_COUNT,
                        DEFAULT_RETRY_DELAY_MILLIS,
                        DEFAULT_RETRY_DELAY_STRATEGY))
                .subscribeOn(provider.io())
                .observeOn(provider.ui())
                .subscribe(
                        giphyResponse -> {
                            if (!view.isActive()) {
                                return;
                            }
                            // Iterate over data from response and grab the urls
                            view.addImages(giphyResponse);
                        },
                        e -> Log.e(TAG, "onError", e)
                )
        );
    }

}
