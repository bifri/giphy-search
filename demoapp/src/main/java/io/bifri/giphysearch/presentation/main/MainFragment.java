package io.bifri.giphysearch.presentation.main;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.MenuItemCompat.OnActionExpandListener;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.squareup.leakcanary.RefWatcher;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.bifri.giphysearch.App;
import io.bifri.giphysearch.R;
import io.bifri.giphysearch.data.rest.giphypojo.GiphyResponse;
import io.bifri.giphysearch.data.rest.repository.GiphyApiClient;
import io.bifri.giphysearch.data.rest.repository.ImageRepository;
import io.bifri.giphysearch.databinding.DialogPreviewBinding;
import io.bifri.giphysearch.databinding.FragmentMainBinding;
import io.bifri.giphysearch.presentation.adapter.GifAdapter;
import io.bifri.giphysearch.presentation.adapter.GifItemDecoration;
import io.bifri.giphysearch.presentation.adapter.model.ImageInfoModel;
import io.bifri.giphysearch.util.rx.binding.RxCompatSearchView;
import java8.util.stream.StreamSupport;

import static io.bifri.giphysearch.util.Constants.SEARCH_INPUT_DELAY_IN_MILLIS;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * Main Fragment of the application that displays the Recylcerview of Gif images.
 *
 * @author <a href="mailto:jaredsburrows@gmail.com">Jared Burrows</a>
 */
public final class MainFragment extends Fragment
        implements IMainView, GifAdapter.OnItemClickListener {
    private static final String TAG = MainFragment.class.getSimpleName();

    private GridLayoutManager layoutManager;
    private GifItemDecoration itemOffsetDecoration;
    private GifAdapter adapter;
    private boolean hasSearched;
    private AppCompatDialog dialog;
    private int previousTotal = 0;
    private boolean loading = true;
    private int visibleThreshold = 5;
    private int firstVisibleItem;
    private int visibleItemCount;
    private int totalItemCount;
    private int nextOffset;
    private TextView dialogText;
    private ProgressBar progressBar;
    private ImageView imageView;
    private IMainPresenter presenter;
    @Inject RefWatcher refWatcher;
    @Inject ImageRepository repository;

    //
    // Contract
    //

    @Override public void setPresenter(IMainPresenter presenter) {
        this.presenter = presenter;
    }

    @Override public void clearImages() {
        adapter.clear();
    }

    @Override public void addImages(GiphyResponse response) {
        try {
            nextOffset = response.getPagination().getOffset()
                    + GiphyApiClient.DEFAULT_LIMIT_COUNT;
            adapter.addAll(gifUrls(response));
        } catch (NullPointerException e) {
            if (Log.isLoggable(TAG, Log.WARN)) {
                Log.w(TAG, "NPE while adding images from response: " + response);
            }
        }
    }

    private List<ImageInfoModel> gifUrls(GiphyResponse response) {
        List<ImageInfoModel> gifUrls = new ArrayList<>();
        StreamSupport.stream(response.getData())
                .forEach(datum -> {
                    try {
                        gifUrls.add(new ImageInfoModel.Builder()
                                .url(datum.getImages().getFixedWidth().getUrl())
                                .previewUrl(datum.getImages().getFixedWidthStill().getUrl())
                                .build());
                    } catch (NullPointerException e) {
                        Log.e(TAG, "NPE while getting fixed width image url from " + datum);
                    }
                });
        return gifUrls;
    }

    @Override public void showDialog(ImageInfoModel imageInfo) {
        showImageDialog(imageInfo);
    }

    @Override public boolean isActive() {
        return isAdded();
    }

    //
    // GifAdapter
    //

    @Override public void onClick(ImageInfoModel imageInfo) {
        showDialog(imageInfo);
    }

    //
    // Fragment
    //

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Injection dependencies
        ((App) getActivity().getApplication()).activityComponent.inject(this);

        setHasOptionsMenu(true);

        adapter = new GifAdapter(this, repository);
        adapter.setHasStableIds(true);
    }

    @Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                                       @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        final FragmentMainBinding b =
                FragmentMainBinding.inflate(inflater, container, false);

        // Setup RecyclerView
        layoutManager = (GridLayoutManager) b.recyclerView.getLayoutManager();
        itemOffsetDecoration = new GifItemDecoration(
                getActivity().getResources().getDimensionPixelSize(R.dimen.gif_adapter_item_offset),
                layoutManager.getSpanCount());
        b.recyclerView.addItemDecoration(itemOffsetDecoration);
        b.recyclerView.setAdapter(adapter);
        b.recyclerView.setHasFixedSize(true);
        // http://stackoverflow.com/questions/30511890/does-glide-queue-up-every-image-request-recyclerview-loads-are-very-slow-when-s#comment49135977_30511890
        b.recyclerView.getRecycledViewPool()
                .setMaxRecycledViews(0, layoutManager.getSpanCount() * 2); // default 5
        b.recyclerView.setItemViewCacheSize(GiphyApiClient.DEFAULT_LIMIT_COUNT);
        b.recyclerView.setDrawingCacheEnabled(true);
        b.recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        // FIXME: move listener to separate class
        b.recyclerView.addOnScrollListener(new OnScrollListener() {
            @Override public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy <= 0) {
                    return;
                }
                // Continuous scrolling
                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                firstVisibleItem = layoutManager.findFirstVisibleItemPosition();

                if (loading && (totalItemCount > previousTotal)) {
                    loading = false;
                    previousTotal = totalItemCount;
                }

                if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem
                        + visibleThreshold)) {
                    presenter.loadTrendingImages(nextOffset);
                    loading = true;
                }
            }
        });

        // Custom view for Dialog
        final DialogPreviewBinding previewBinding =
                DataBindingUtil.inflate(inflater, R.layout.dialog_preview, null, false);

        // Customize Dialog
        dialog = new AppCompatDialog(getContext());
        dialog.setContentView(previewBinding.getRoot());
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setOnDismissListener(dialog1 -> {
            // https://github.com/bumptech/glide/issues/624#issuecomment-140134792
            Glide.clear(imageView);  // Forget view, try to free resources
            imageView.setImageDrawable(null);
            progressBar.setVisibility(View.VISIBLE); // Make sure to show progress when loading new view
        });

        // Dialog views
        dialogText = previewBinding.gifDialogTitle;
        progressBar = previewBinding.gifDialogProgress;
        imageView = previewBinding.gifDialogImage;

        // Load initial images
        presenter.loadTrendingImages(nextOffset);

        return b.getRoot();
    }

    @Override public void onPause() {
        super.onPause();

        presenter.unsubscribe();
    }

    @Override public void onDestroy() {
        super.onDestroy();

        refWatcher.watch(this, TAG);
    }

    @Override public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_fragment_main, menu);

        final MenuItem menuItem = menu.findItem(R.id.menu_search);

        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setQueryHint(searchView.getContext().getString(R.string.search_gifs));

        // Set contextual action on search icon click
        MenuItemCompat.setOnActionExpandListener(menuItem, new OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // When search is closed, go back to trending results
                if (hasSearched) {
                    cleanScrollListener();
                    presenter.clearImages();
                    presenter.loadTrendingImages(nextOffset);
                    hasSearched = false;
                }
                return true;
            }
        });

        RxCompatSearchView.queryTextChanges(searchView)
                .debounce(SEARCH_INPUT_DELAY_IN_MILLIS, MILLISECONDS)
                .filter(searchString -> !TextUtils.isEmpty(searchString))
                .distinctUntilChanged()
                .subscribe(this::onQueryTextChanged);
    }

    public void onQueryTextChanged(String searchString) {
        presenter.clearImages();
        presenter.loadSearchImages(searchString, nextOffset);
        hasSearched = true;
    }

    // TODO: clean up moving all variables below in separate listener class
    private void cleanScrollListener() {
        nextOffset = 0;
        previousTotal = 0;
        loading = true;
        firstVisibleItem = 0;
        visibleItemCount = 0;
        totalItemCount = 0;
    }

    @Override public void onResume() {
        super.onResume();

        presenter.subscribe();
    }

    private void showImageDialog(ImageInfoModel imageInfo) {
        dialog.show();
        // Remove "white" background for dialog
        dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);

        // Load associated text
        dialogText.setText(imageInfo.url());
        dialogText.setVisibility(View.VISIBLE);

        // Load image
        repository.load(imageInfo.url())
//              .thumbnail(repository.load(imageInfo.previewUrl()))
                .listener(new RequestListener<Object, GifDrawable>() {
                    @Override public boolean onException(
                            Exception e,
                            Object model,
                            Target<GifDrawable> target,
                            boolean isFirstResource) {
                        // Hide progressbar
                        progressBar.setVisibility(View.GONE);
                        if (Log.isLoggable(TAG, Log.INFO)) Log.i(TAG, "finished loading\t" + model);

                        return false;
                    }

                    @Override public boolean onResourceReady(
                            GifDrawable resource,
                            Object model,
                            Target<GifDrawable> target,
                            boolean isFromMemoryCache, boolean isFirstResource) {
                        // Hide progressbar
                        progressBar.setVisibility(View.GONE);
                        if (Log.isLoggable(TAG, Log.INFO)) Log.i(TAG, "finished loading\t" + model);

                        return false;
                    }
                })
                .into(imageView);
    }
}
