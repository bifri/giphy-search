package io.bifri.giphysearch.presentation;

import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * @author <a href="mailto:jaredsburrows@gmail.com">Jared Burrows</a>
 */
public final class SchedulerProvider implements IBaseSchedulerProvider {
  @Override public Scheduler io() {
    return Schedulers.io();
  }

  @Override public Scheduler ui() {
    return AndroidSchedulers.mainThread();
  }
}
