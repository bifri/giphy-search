package io.bifri.giphysearch.presentation.di.module;

import android.app.Application;
import io.bifri.giphysearch.presentation.di.scope.PerActivity;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;
import dagger.Module;
import dagger.Provides;

/**
 * @author <a href="mailto:jaredsburrows@gmail.com">Jared Burrows</a>
 */
@Module
public final class LeakCanaryModule {
  @Provides @PerActivity RefWatcher provideRefWatcher(Application application) {
    return LeakCanary.install(application);
  }
}
