package io.bifri.giphysearch.presentation;

/**
 * @author <a href="mailto:jaredsburrows@gmail.com">Jared Burrows</a>
 */
public interface IBaseView<T> {
  void setPresenter(T presenter);
}
