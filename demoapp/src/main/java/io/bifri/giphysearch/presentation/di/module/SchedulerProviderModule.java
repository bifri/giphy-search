package io.bifri.giphysearch.presentation.di.module;

import io.bifri.giphysearch.presentation.di.scope.PerActivity;
import io.bifri.giphysearch.presentation.SchedulerProvider;
import dagger.Module;
import dagger.Provides;

/**
 * @author <a href="mailto:jaredsburrows@gmail.com">Jared Burrows</a>
 */
@Module
public final class SchedulerProviderModule {
  @Provides @PerActivity SchedulerProvider providerSchedulerProvider() {
    return new SchedulerProvider();
  }
}
