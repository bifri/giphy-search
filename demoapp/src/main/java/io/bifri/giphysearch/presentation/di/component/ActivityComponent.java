package io.bifri.giphysearch.presentation.di.component;

import io.bifri.giphysearch.presentation.di.module.GlideModule;
import io.bifri.giphysearch.presentation.di.module.LeakCanaryModule;
import io.bifri.giphysearch.presentation.di.module.NetModule;
import io.bifri.giphysearch.presentation.di.module.GiphyModule;
import io.bifri.giphysearch.presentation.di.module.SchedulerProviderModule;
import io.bifri.giphysearch.presentation.di.scope.PerActivity;
import io.bifri.giphysearch.presentation.main.MainActivity;
import io.bifri.giphysearch.presentation.main.MainFragment;
import io.bifri.giphysearch.presentation.adapter.GifAdapter;
import dagger.Component;

/**
 * @author <a href="mailto:jaredsburrows@gmail.com">Jared Burrows</a>
 */
@PerActivity
@Component(dependencies = AppComponent.class, modules = {NetModule.class, GiphyModule.class,
  GlideModule.class, SchedulerProviderModule.class, LeakCanaryModule.class})
public interface ActivityComponent {
  // Injections
  void inject(MainActivity mainActivity);
  void inject(MainFragment mainFragment);
  void inject(GifAdapter gifAdapter);

  // Setup components dependencies and modules
  final class Initializer {
    private Initializer() {
    }

    public static ActivityComponent init(AppComponent appComponent) {
      return DaggerActivityComponent.builder()
        .appComponent(appComponent)
        .build();
    }
  }
}
