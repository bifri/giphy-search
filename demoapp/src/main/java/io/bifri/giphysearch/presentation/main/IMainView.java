package io.bifri.giphysearch.presentation.main;

import io.bifri.giphysearch.data.rest.giphypojo.GiphyResponse;
import io.bifri.giphysearch.presentation.IBaseView;
import io.bifri.giphysearch.presentation.adapter.model.ImageInfoModel;

/**
 * @author <a href="mailto:jaredsburrows@gmail.com">Jared Burrows</a>
 */
interface IMainView extends IBaseView<IMainPresenter> {
  void clearImages();
  void addImages(GiphyResponse response);
  void showDialog(ImageInfoModel url);
  boolean isActive();
}
