package io.bifri.giphysearch.presentation.di.module;

import android.content.Context;
import io.bifri.giphysearch.data.rest.repository.ImageRepository;
import io.bifri.giphysearch.presentation.di.scope.PerActivity;
import dagger.Module;
import dagger.Provides;

/**
 * @author <a href="mailto:jaredsburrows@gmail.com">Jared Burrows</a>
 */
@Module
public class GlideModule {
  @Provides @PerActivity
  protected ImageRepository provideImageRepository(Context context) {
    return new ImageRepository(context);
  }
}
