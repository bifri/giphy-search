package io.bifri.giphysearch.presentation.main;

import io.bifri.giphysearch.presentation.IBasePresenter;

/**
 * @author <a href="mailto:jaredsburrows@gmail.com">Jared Burrows</a>
 */
interface IMainPresenter extends IBasePresenter {
  void clearImages();
  void loadTrendingImages(int offset);
  void loadSearchImages(String searchString, int offset);
}
