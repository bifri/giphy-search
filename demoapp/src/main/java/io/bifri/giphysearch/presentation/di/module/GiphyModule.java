package io.bifri.giphysearch.presentation.di.module;

import io.bifri.giphysearch.data.rest.repository.GiphyApiClient;
import io.bifri.giphysearch.presentation.di.scope.PerActivity;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * @author <a href="mailto:jaredsburrows@gmail.com">Jared Burrows</a>
 */
@Module
public class GiphyModule {
    @Provides @PerActivity
    protected GiphyApiClient provideGiphyApi(Retrofit.Builder builder) {
        return builder.baseUrl(GiphyApiClient.BASE_URL)
                .build()
                .create(GiphyApiClient.class);
    }
}
