package io.bifri.giphysearch.presentation;

import rx.Scheduler;

/**
 * @author <a href="mailto:jaredsburrows@gmail.com">Jared Burrows</a>
 */
public interface IBaseSchedulerProvider {
  Scheduler io();
  Scheduler ui();
}
