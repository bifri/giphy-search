package io.bifri.giphysearch.presentation;

/**
 * @author <a href="mailto:jaredsburrows@gmail.com">Jared Burrows</a>
 */
public interface IBasePresenter {
  void subscribe();
  void unsubscribe();
}
