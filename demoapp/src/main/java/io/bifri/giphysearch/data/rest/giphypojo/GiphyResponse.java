
package io.bifri.giphysearch.data.rest.giphypojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GiphyResponse {

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("meta")
    @Expose
    private Meta meta;
    @SerializedName("pagination")
    @Expose
    private Pagination pagination;

    /**
     * No args constructor for use in serialization
     * 
     */
    public GiphyResponse() {
    }

    /**
     * 
     * @param data
     * @param pagination
     * @param meta
     */
    public GiphyResponse(List<Datum> data, Meta meta, Pagination pagination) {
        super();
        this.data = data;
        this.meta = meta;
        this.pagination = pagination;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    @Override
    public String toString() {
        return "SearchResponse{" +
                "data=" + data +
                ", meta=" + meta +
                ", pagination=" + pagination +
                '}';
    }

}
