package io.bifri.giphysearch.data.rest.repository;

import io.bifri.giphysearch.data.rest.giphypojo.GiphyResponse;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface GiphyApiClient {

  String BASE_URL = "http://api.giphy.com/v1/gifs/";

  String PUBLIC_BETA_API_KEY = "dc6zaTOxFJmzC";

  int DEFAULT_LIMIT_COUNT = 24;

  @GET("trending?api_key=" + PUBLIC_BETA_API_KEY)
  Observable<GiphyResponse> trending(
          @Query("limit") int limit,
          @Query("offset") int offset);

  @GET("search?api_key=" + PUBLIC_BETA_API_KEY)
  Observable<GiphyResponse> search(
          @Query("q") String searchQuery,
          @Query("limit") int limit,
          @Query("offset") int offset);

}
