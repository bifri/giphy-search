
package io.bifri.giphysearch.data.rest.giphypojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("bitly_gif_url")
    @Expose
    private String bitlyGifUrl;
    @SerializedName("bitly_url")
    @Expose
    private String bitlyUrl;
    @SerializedName("embed_url")
    @Expose
    private String embedUrl;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("rating")
    @Expose
    private String rating;
    @SerializedName("caption")
    @Expose
    private String caption;
    @SerializedName("content_url")
    @Expose
    private String contentUrl;
    @SerializedName("source_tld")
    @Expose
    private String sourceTld;
    @SerializedName("source_post_url")
    @Expose
    private String sourcePostUrl;
    @SerializedName("import_datetime")
    @Expose
    private String importDatetime;
    @SerializedName("trending_datetime")
    @Expose
    private String trendingDatetime;
    @SerializedName("images")
    @Expose
    private Images images;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Datum() {
    }

    /**
     * 
     * @param importDatetime
     * @param bitlyGifUrl
     * @param contentUrl
     * @param embedUrl
     * @param sourceTld
     * @param caption
     * @param type
     * @param trendingDatetime
     * @param url
     * @param bitlyUrl
     * @param id
     * @param username
     * @param source
     * @param sourcePostUrl
     * @param images
     * @param slug
     * @param rating
     */
    public Datum(String type, String id, String slug, String url, String bitlyGifUrl, String bitlyUrl, String embedUrl, String username, String source, String rating, String caption, String contentUrl, String sourceTld, String sourcePostUrl, String importDatetime, String trendingDatetime, Images images) {
        super();
        this.type = type;
        this.id = id;
        this.slug = slug;
        this.url = url;
        this.bitlyGifUrl = bitlyGifUrl;
        this.bitlyUrl = bitlyUrl;
        this.embedUrl = embedUrl;
        this.username = username;
        this.source = source;
        this.rating = rating;
        this.caption = caption;
        this.contentUrl = contentUrl;
        this.sourceTld = sourceTld;
        this.sourcePostUrl = sourcePostUrl;
        this.importDatetime = importDatetime;
        this.trendingDatetime = trendingDatetime;
        this.images = images;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBitlyGifUrl() {
        return bitlyGifUrl;
    }

    public void setBitlyGifUrl(String bitlyGifUrl) {
        this.bitlyGifUrl = bitlyGifUrl;
    }

    public String getBitlyUrl() {
        return bitlyUrl;
    }

    public void setBitlyUrl(String bitlyUrl) {
        this.bitlyUrl = bitlyUrl;
    }

    public String getEmbedUrl() {
        return embedUrl;
    }

    public void setEmbedUrl(String embedUrl) {
        this.embedUrl = embedUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getContentUrl() {
        return contentUrl;
    }

    public void setContentUrl(String contentUrl) {
        this.contentUrl = contentUrl;
    }

    public String getSourceTld() {
        return sourceTld;
    }

    public void setSourceTld(String sourceTld) {
        this.sourceTld = sourceTld;
    }

    public String getSourcePostUrl() {
        return sourcePostUrl;
    }

    public void setSourcePostUrl(String sourcePostUrl) {
        this.sourcePostUrl = sourcePostUrl;
    }

    public String getImportDatetime() {
        return importDatetime;
    }

    public void setImportDatetime(String importDatetime) {
        this.importDatetime = importDatetime;
    }

    public String getTrendingDatetime() {
        return trendingDatetime;
    }

    public void setTrendingDatetime(String trendingDatetime) {
        this.trendingDatetime = trendingDatetime;
    }

    public Images getImages() {
        return images;
    }

    public void setImages(Images images) {
        this.images = images;
    }

    @Override
    public String toString() {
        return "Datum{" +
                "type='" + type + '\'' +
                ", id='" + id + '\'' +
                ", slug='" + slug + '\'' +
                ", url='" + url + '\'' +
                ", bitlyGifUrl='" + bitlyGifUrl + '\'' +
                ", bitlyUrl='" + bitlyUrl + '\'' +
                ", embedUrl='" + embedUrl + '\'' +
                ", username='" + username + '\'' +
                ", source='" + source + '\'' +
                ", rating='" + rating + '\'' +
                ", caption='" + caption + '\'' +
                ", contentUrl='" + contentUrl + '\'' +
                ", sourceTld='" + sourceTld + '\'' +
                ", sourcePostUrl='" + sourcePostUrl + '\'' +
                ", importDatetime='" + importDatetime + '\'' +
                ", trendingDatetime='" + trendingDatetime + '\'' +
                ", images=" + images +
                '}';
    }

}
