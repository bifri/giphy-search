
package io.bifri.giphysearch.data.rest.giphypojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Images {

    @SerializedName("fixed_height")
    @Expose
    private FixedHeight fixedHeight;
    @SerializedName("fixed_height_still")
    @Expose
    private FixedHeightStill fixedHeightStill;
    @SerializedName("fixed_height_downsampled")
    @Expose
    private FixedHeightDownsampled fixedHeightDownsampled;
    @SerializedName("fixed_width")
    @Expose
    private FixedWidth fixedWidth;
    @SerializedName("fixed_width_still")
    @Expose
    private FixedWidthStill fixedWidthStill;
    @SerializedName("fixed_width_downsampled")
    @Expose
    private FixedWidthDownsampled fixedWidthDownsampled;
    @SerializedName("fixed_height_small")
    @Expose
    private FixedHeightSmall fixedHeightSmall;
    @SerializedName("fixed_height_small_still")
    @Expose
    private FixedHeightSmallStill fixedHeightSmallStill;
    @SerializedName("fixed_width_small")
    @Expose
    private FixedWidthSmall fixedWidthSmall;
    @SerializedName("fixed_width_small_still")
    @Expose
    private FixedWidthSmallStill fixedWidthSmallStill;
    @SerializedName("downsized")
    @Expose
    private Downsized downsized;
    @SerializedName("downsized_still")
    @Expose
    private DownsizedStill downsizedStill;
    @SerializedName("downsized_large")
    @Expose
    private DownsizedLarge downsizedLarge;
    @SerializedName("original")
    @Expose
    private Original original;
    @SerializedName("original_still")
    @Expose
    private OriginalStill originalStill;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Images() {
    }

    /**
     * 
     * @param originalStill
     * @param fixedHeightSmallStill
     * @param fixedWidth
     * @param downsizedLarge
     * @param downsizedStill
     * @param fixedWidthSmallStill
     * @param fixedWidthSmall
     * @param fixedHeight
     * @param fixedWidthStill
     * @param fixedWidthDownsampled
     * @param fixedHeightSmall
     * @param original
     * @param fixedHeightDownsampled
     * @param fixedHeightStill
     * @param downsized
     */
    public Images(FixedHeight fixedHeight, FixedHeightStill fixedHeightStill, FixedHeightDownsampled fixedHeightDownsampled, FixedWidth fixedWidth, FixedWidthStill fixedWidthStill, FixedWidthDownsampled fixedWidthDownsampled, FixedHeightSmall fixedHeightSmall, FixedHeightSmallStill fixedHeightSmallStill, FixedWidthSmall fixedWidthSmall, FixedWidthSmallStill fixedWidthSmallStill, Downsized downsized, DownsizedStill downsizedStill, DownsizedLarge downsizedLarge, Original original, OriginalStill originalStill) {
        super();
        this.fixedHeight = fixedHeight;
        this.fixedHeightStill = fixedHeightStill;
        this.fixedHeightDownsampled = fixedHeightDownsampled;
        this.fixedWidth = fixedWidth;
        this.fixedWidthStill = fixedWidthStill;
        this.fixedWidthDownsampled = fixedWidthDownsampled;
        this.fixedHeightSmall = fixedHeightSmall;
        this.fixedHeightSmallStill = fixedHeightSmallStill;
        this.fixedWidthSmall = fixedWidthSmall;
        this.fixedWidthSmallStill = fixedWidthSmallStill;
        this.downsized = downsized;
        this.downsizedStill = downsizedStill;
        this.downsizedLarge = downsizedLarge;
        this.original = original;
        this.originalStill = originalStill;
    }

    public FixedHeight getFixedHeight() {
        return fixedHeight;
    }

    public void setFixedHeight(FixedHeight fixedHeight) {
        this.fixedHeight = fixedHeight;
    }

    public FixedHeightStill getFixedHeightStill() {
        return fixedHeightStill;
    }

    public void setFixedHeightStill(FixedHeightStill fixedHeightStill) {
        this.fixedHeightStill = fixedHeightStill;
    }

    public FixedHeightDownsampled getFixedHeightDownsampled() {
        return fixedHeightDownsampled;
    }

    public void setFixedHeightDownsampled(FixedHeightDownsampled fixedHeightDownsampled) {
        this.fixedHeightDownsampled = fixedHeightDownsampled;
    }

    public FixedWidth getFixedWidth() {
        return fixedWidth;
    }

    public void setFixedWidth(FixedWidth fixedWidth) {
        this.fixedWidth = fixedWidth;
    }

    public FixedWidthStill getFixedWidthStill() {
        return fixedWidthStill;
    }

    public void setFixedWidthStill(FixedWidthStill fixedWidthStill) {
        this.fixedWidthStill = fixedWidthStill;
    }

    public FixedWidthDownsampled getFixedWidthDownsampled() {
        return fixedWidthDownsampled;
    }

    public void setFixedWidthDownsampled(FixedWidthDownsampled fixedWidthDownsampled) {
        this.fixedWidthDownsampled = fixedWidthDownsampled;
    }

    public FixedHeightSmall getFixedHeightSmall() {
        return fixedHeightSmall;
    }

    public void setFixedHeightSmall(FixedHeightSmall fixedHeightSmall) {
        this.fixedHeightSmall = fixedHeightSmall;
    }

    public FixedHeightSmallStill getFixedHeightSmallStill() {
        return fixedHeightSmallStill;
    }

    public void setFixedHeightSmallStill(FixedHeightSmallStill fixedHeightSmallStill) {
        this.fixedHeightSmallStill = fixedHeightSmallStill;
    }

    public FixedWidthSmall getFixedWidthSmall() {
        return fixedWidthSmall;
    }

    public void setFixedWidthSmall(FixedWidthSmall fixedWidthSmall) {
        this.fixedWidthSmall = fixedWidthSmall;
    }

    public FixedWidthSmallStill getFixedWidthSmallStill() {
        return fixedWidthSmallStill;
    }

    public void setFixedWidthSmallStill(FixedWidthSmallStill fixedWidthSmallStill) {
        this.fixedWidthSmallStill = fixedWidthSmallStill;
    }

    public Downsized getDownsized() {
        return downsized;
    }

    public void setDownsized(Downsized downsized) {
        this.downsized = downsized;
    }

    public DownsizedStill getDownsizedStill() {
        return downsizedStill;
    }

    public void setDownsizedStill(DownsizedStill downsizedStill) {
        this.downsizedStill = downsizedStill;
    }

    public DownsizedLarge getDownsizedLarge() {
        return downsizedLarge;
    }

    public void setDownsizedLarge(DownsizedLarge downsizedLarge) {
        this.downsizedLarge = downsizedLarge;
    }

    public Original getOriginal() {
        return original;
    }

    public void setOriginal(Original original) {
        this.original = original;
    }

    public OriginalStill getOriginalStill() {
        return originalStill;
    }

    public void setOriginalStill(OriginalStill originalStill) {
        this.originalStill = originalStill;
    }

    @Override
    public String toString() {
        return "Images{" +
                "fixedHeight=" + fixedHeight +
                ", fixedHeightStill=" + fixedHeightStill +
                ", fixedHeightDownsampled=" + fixedHeightDownsampled +
                ", fixedWidth=" + fixedWidth +
                ", fixedWidthStill=" + fixedWidthStill +
                ", fixedWidthDownsampled=" + fixedWidthDownsampled +
                ", fixedHeightSmall=" + fixedHeightSmall +
                ", fixedHeightSmallStill=" + fixedHeightSmallStill +
                ", fixedWidthSmall=" + fixedWidthSmall +
                ", fixedWidthSmallStill=" + fixedWidthSmallStill +
                ", downsized=" + downsized +
                ", downsizedStill=" + downsizedStill +
                ", downsizedLarge=" + downsizedLarge +
                ", original=" + original +
                ", originalStill=" + originalStill +
                '}';
    }

}
