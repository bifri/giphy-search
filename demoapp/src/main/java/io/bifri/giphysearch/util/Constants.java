package io.bifri.giphysearch.util;


public class Constants {

    public static final long SEARCH_INPUT_DELAY_IN_MILLIS = 200;

    private Constants() {

    }
}
