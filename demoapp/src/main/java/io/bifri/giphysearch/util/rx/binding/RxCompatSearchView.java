package io.bifri.giphysearch.util.rx.binding;


import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;
import android.support.v7.widget.SearchView;

import rx.Emitter.BackpressureMode;
import rx.Observable;

import static com.jakewharton.rxbinding.internal.Preconditions.checkNotNull;

public class RxCompatSearchView {

    /**
     * Create an observable of character sequences for query text changes on {@code view}.
     * <p>
     * <em>Warning:</em> The created observable keeps a strong reference to {@code view}. Unsubscribe
     * to free this reference.
     * <p>
     * <em>Note:</em> A value will be emitted immediately on subscribe.
     */
    @CheckResult
    @NonNull
    public static Observable<String> queryTextChanges(@NonNull SearchView view) {
        checkNotNull(view, "view == null");
        return Observable.create(new SearchViewQueryTextChangesOnSubscribe(view), BackpressureMode.BUFFER);
    }

}
