package io.bifri.giphysearch.util.rx.binding;


import android.support.v7.widget.SearchView;

import rx.Emitter;
import rx.functions.Action1;
import rx.functions.Cancellable;

import static rx.android.MainThreadSubscription.verifyMainThread;

final class SearchViewQueryTextChangesOnSubscribe implements Action1<Emitter<String>> {
    final SearchView view;

    SearchViewQueryTextChangesOnSubscribe(SearchView view) {
        this.view = view;
    }

    @Override public void call(final Emitter<String> emitter) {
        verifyMainThread();

        SearchView.OnQueryTextListener listener = new SearchView.OnQueryTextListener() {
            @Override public boolean onQueryTextChange(String s) {
                emitter.onNext(s);
                return true;
            }

            @Override public boolean onQueryTextSubmit(String query) {
                return false;
            }
        };

        emitter.setCancellation(new Cancellable() {
            @Override
            public void cancel() throws Exception {
                view.setOnQueryTextListener(null);
            }
        });

        view.setOnQueryTextListener(listener);

        // Emit initial value.
        emitter.onNext(view.getQuery().toString());
    }
}