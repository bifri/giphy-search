package io.bifri.giphysearch;

import android.app.Application;

import io.bifri.giphysearch.presentation.di.component.ActivityComponent;
import io.bifri.giphysearch.presentation.di.component.AppComponent;

/**
 * @author <a href="mailto:jaredsburrows@gmail.com">Jared Burrows</a>
 */
public class App extends Application {
  public AppComponent appComponent;
  public ActivityComponent activityComponent;

  @Override public void onCreate() {
    super.onCreate();

    // Setup components
    appComponent = AppComponent.Initializer.init(this);
    activityComponent = ActivityComponent.Initializer.init(appComponent);
  }
}
